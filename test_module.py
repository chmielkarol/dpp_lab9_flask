import unittest
import requests


class TestSimple(unittest.TestCase):

    def test_helloworld(self):
        req = requests.get("http://localhost:5000/")
        self.assertEqual("Hello, World!", req.text)

    def test_ok_2(self):
        bar = True
        self.assertTrue(bar)

    def test_fail(self):
        baz = False
        self.assertTrue(baz)


if __name__ == '__main__':
    unittest.main()